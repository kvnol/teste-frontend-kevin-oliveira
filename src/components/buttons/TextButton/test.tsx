import { render, screen } from '@testing-library/react'
import TextButton from '.'

describe('<TextButton />', () => {
  it('should render the component', () => {
    render(<TextButton title="Label"></TextButton>)

    expect(screen.getByRole('button')).toBeTruthy()
  })

  it('should render correct title', () => {
    render(<TextButton title="Label"></TextButton>)

    expect(screen.getByRole('button').getAttribute('aria-label')).toEqual(
      'Label'
    )
  })
})
