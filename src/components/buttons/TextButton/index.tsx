import styles from './TextButton.module.scss'
import { ChevronRight } from '@styled-icons/boxicons-solid'

type TextButtonProps = {
  title: string
}

export default function TextButton({ title }: TextButtonProps) {
  return (
    <button aria-label={title} className={styles.button}>
      <span>{title}</span>
      <ChevronRight width={18} height={18} />
    </button>
  )
}
