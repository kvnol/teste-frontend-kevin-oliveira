import { render, screen } from '@testing-library/react'
import IconButton from '.'

describe('<IconButton />', () => {
  it('should render the component', () => {
    render(
      <IconButton title="Label" disabled={false}>
        <i />
      </IconButton>
    )

    expect(screen.getByRole('button')).toBeTruthy()
  })

  it('should render correct title', () => {
    render(
      <IconButton title="Label" disabled={false}>
        <i />
      </IconButton>
    )

    expect(screen.getByRole('button').getAttribute('aria-label')).toEqual(
      'Label'
    )
  })

  it('should disable when receive disabled=true', () => {
    render(
      <IconButton title="Label" disabled={true}>
        <i />
      </IconButton>
    )

    expect(screen.getByRole('button')).toBeDisabled()
  })

  it('should render correct icon', () => {
    render(
      <IconButton title="Label" disabled={true}>
        <i className="icon" />
      </IconButton>
    )

    expect(screen.getByRole('button').querySelector('i')).toHaveClass('icon')
  })
})
