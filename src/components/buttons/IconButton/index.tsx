import styles from './IconButton.module.scss'

type IconButtonProps = {
  title: string
  disabled: boolean
  children: React.ReactNode
}

export default function IconButton({
  title,
  disabled,
  children
}: IconButtonProps) {
  return (
    <button disabled={disabled} aria-label={title} className={styles.button}>
      {children}
    </button>
  )
}
