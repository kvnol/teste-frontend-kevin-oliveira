import Image from 'next/image'
import styles from './CardVideoHighlight.module.scss'
import Link from 'next/link'
import { CaretRightCircle } from '@styled-icons/boxicons-solid'

type CardVideoHighlightProps = {
  thumbnail: string
  hat: string
  title: string
  link: string
}

export default function CardVideoHighlight({
  thumbnail,
  hat,
  title,
  link
}: CardVideoHighlightProps) {
  return (
    <Link href={link} target="_blank">
      <article className={styles.card}>
        <figure className={styles['card-figure']}>
          <Image
            src={thumbnail}
            alt={title}
            width={327}
            height={204}
            loading="lazy"
          />
        </figure>
        <div className={styles['card-content']}>
          <div className={styles['card-content__button']}>
            <CaretRightCircle />
          </div>
          <h2 className={styles['card-content__hat']}>{hat}</h2>
          <h3 className={styles['card-content__title']}>{title}</h3>
        </div>
      </article>
    </Link>
  )
}
