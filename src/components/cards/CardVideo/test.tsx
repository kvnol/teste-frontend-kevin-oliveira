import { render, screen } from '@testing-library/react'
import CardVideo from '.'

describe('<CardVideo />', () => {
  it('should render the component', () => {
    render(
      <CardVideo
        title="title"
        thumbnail="https://teste-frontend.seox.com.br/wp-content/uploads/2023/05/20304564-011e-3efe-a0da-22a9db53511e.jpg"
        hat="category"
        link="/video/1"
      />
    )

    expect(screen.getByRole('article')).toBeTruthy()
  })

  it('should render correct title text', () => {
    render(
      <CardVideo
        title="title"
        thumbnail="https://teste-frontend.seox.com.br/wp-content/uploads/2023/05/20304564-011e-3efe-a0da-22a9db53511e.jpg"
        hat="Hat"
        link="/video/1"
      />
    )

    expect(screen.getByRole('article').querySelector('h3')).toHaveTextContent(
      'title'
    )
  })

  it('should render correct hat text', () => {
    render(
      <CardVideo
        title="title"
        thumbnail="https://teste-frontend.seox.com.br/wp-content/uploads/2023/05/20304564-011e-3efe-a0da-22a9db53511e.jpg"
        hat="category"
        link="/video/1"
      />
    )

    expect(screen.getByRole('article').querySelector('h2')).toHaveTextContent(
      'category'
    )
  })

  it('should render correct link', () => {
    render(
      <CardVideo
        title="title"
        thumbnail="https://teste-frontend.seox.com.br/wp-content/uploads/2023/05/20304564-011e-3efe-a0da-22a9db53511e.jpg"
        hat="category"
        link="/video/1"
      />
    )

    expect(screen.getByRole('link')).toHaveAttribute('href', '/video/1')
  })

  it('should render correct thumbnail', () => {
    render(
      <CardVideo
        title="title"
        thumbnail="https://teste-frontend.seox.com.br/wp-content/uploads/2023/05/20304564-011e-3efe-a0da-22a9db53511e.jpg"
        hat="category"
        link="/video/1"
      />
    )

    expect(screen.getByRole('img').getAttribute('src')).toContain(
      'teste-frontend.seox.com.br'
    )
  })
})
