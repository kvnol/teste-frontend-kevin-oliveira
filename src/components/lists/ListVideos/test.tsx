import { fireEvent, render, screen } from '@testing-library/react'
import ListVideos from './ListVideos'

describe('<ListVideos />', () => {
  it('should render the component', () => {
    render(
      <ListVideos>
        <li></li>
      </ListVideos>
    )

    expect(screen.getByRole('list')).toBeTruthy()
  })

  it('should render correct inner items', () => {
    render(
      <ListVideos>
        <li>Lorem ipsum</li>
      </ListVideos>
    )

    expect(screen.getByRole('list').querySelector('li')).toBeTruthy()
  })
})
