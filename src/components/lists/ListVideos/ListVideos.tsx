import styles from './ListVideos.module.scss'

type ListVideosProps = {
  children: React.ReactNode
}

export default function ListVideos({ children }: ListVideosProps) {
  return <ul className={styles.list}>{children}</ul>
}
