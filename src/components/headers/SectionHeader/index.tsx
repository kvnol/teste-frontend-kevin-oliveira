import styles from './SectionHeader.module.scss'
import IconButton from '../../buttons/IconButton'
import TextButton from '../../buttons/TextButton'
import { ChevronUp, ChevronDown } from '@styled-icons/boxicons-solid'
import Link from 'next/link'
import { useState } from 'react'

type SectionHeaderProps = {
  title: string
  videosLength: number
}

export default function SectionHeader({
  title,
  videosLength
}: SectionHeaderProps) {
  const [index, setIndex] = useState(1)

  return (
    <header role="sectionhead" className={styles.header}>
      <h1 className={styles['header-title']}>{title}</h1>
      <div className={styles['header-actions']}>
        <TextButton title="Veja mais" />

        <div className={styles['header-controls']}>
          <Link
            href={`#video-${!(index === 1) ? index - 1 : index}`}
            onClick={() => (!(index === 1) ? setIndex(index - 1) : '')}
            title="Voltar slide"
          >
            <IconButton disabled={index === 1} title="Voltar slide">
              <ChevronUp />
            </IconButton>
          </Link>
          <Link
            href={`#video-${!(index >= videosLength) ? index + 1 : index}`}
            onClick={() =>
              !(index >= videosLength) ? setIndex(index + 1) : ''
            }
            title="Avançar slide"
          >
            <IconButton disabled={index >= videosLength} title="Avançar slide">
              <ChevronDown />
            </IconButton>
          </Link>
        </div>
      </div>
    </header>
  )
}
