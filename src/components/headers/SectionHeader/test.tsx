import { fireEvent, render, screen } from '@testing-library/react'
import SectionHeader from '.'

describe('<SectionHeader />', () => {
  it('should render the component', () => {
    render(<SectionHeader title="title" videosLength={0} />)

    expect(screen.getByRole('sectionhead')).toBeTruthy()
  })

  it('should render the correct title', () => {
    render(<SectionHeader title="title" videosLength={0} />)

    expect(
      screen.getByRole('sectionhead').querySelector('h1')
    ).toHaveTextContent('title')
  })

  it('should disable prev button when index is === 1', () => {
    render(<SectionHeader title="title" videosLength={8} />)

    expect(screen.getByLabelText('Voltar slide')).toBeDisabled()
  })

  it('should change index when button in clicked', () => {
    render(<SectionHeader title="title" videosLength={3} />)

    fireEvent.click(screen.getByTitle('Avançar slide'))

    expect(screen.getByTitle('Avançar slide')).toHaveAttribute(
      'href',
      '#video-3'
    )
  })

  it('should enable prev button when index is > 1', () => {
    render(<SectionHeader title="title" videosLength={8} />)

    fireEvent.click(screen.getByTitle('Avançar slide'))

    expect(screen.getByLabelText('Voltar slide')).toBeEnabled()
  })

  it('should disable next button when index === 3', () => {
    render(<SectionHeader title="title" videosLength={3} />)

    fireEvent.click(screen.getByTitle('Avançar slide'))
    fireEvent.click(screen.getByTitle('Avançar slide'))

    expect(screen.getByLabelText('Avançar slide')).toBeDisabled()
  })
})
