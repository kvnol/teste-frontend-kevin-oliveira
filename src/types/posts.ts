export type Posts = {
  id: number
  date: string
  slug: string
  status: string
  type: string
  link: string
  title: {
    rendered: string
  }
  content: {
    rendered: string
  }
  excerpt: {
    rendered: string
  }
  acf: {
    hat: string
  }
  _embedded: {
    'wp:featuredmedia': [
      {
        id: number
        title: {
          rendered: string
        }
        media_details: {
          sizes: {
            medium: {
              source_url: string
            }
            full: {
              source_url: string
            }
          }
        }
        source_url: string
      }
    ]
  }
}
