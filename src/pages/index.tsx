import Head from 'next/head'
import styles from '@/styles/pages/Home.module.scss'
import SectionHeader from '@/components/headers/SectionHeader'
import { api } from '@/api/environments'
import { Posts } from '@/types/posts'
import { GetStaticProps } from 'next'
import CardVideoHighlight from '@/components/cards/CardVideoHighlight'
import CardVideo from '@/components/cards/CardVideo'
import ListVideos from '@/components/lists/ListVideos/ListVideos'

type HomeProps = {
  posts: Posts[]
}

export default function Home({ posts }: HomeProps) {
  return (
    <>
      <Head>
        <title>Teste SEOX</title>
        <meta
          name="description"
          content="Teste para o processo seletivo da SEOX. Feito com Next.js!"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <section className={styles['section']}>
          <div className={`${styles['main-wrapper']} ${styles.header}`}>
            <SectionHeader title="Vídeos" videosLength={posts.length - 2} />
          </div>

          <div className={`${styles['main-wrapper']} ${styles.banner}`}>
            {posts.map((post, index) => {
              return index === 0 ? (
                <CardVideoHighlight
                  key={post.id}
                  thumbnail={post._embedded['wp:featuredmedia'][0].source_url}
                  title={post.title.rendered}
                  hat={post.acf.hat}
                  link={post.link}
                />
              ) : (
                ''
              )
            })}
          </div>

          <div className={styles.list}>
            <ListVideos>
              {posts.map((post, index) => {
                return index !== 0 ? (
                  <li key={post.id} id={`video-${index}`}>
                    <CardVideo
                      thumbnail={
                        post._embedded['wp:featuredmedia'][0].media_details
                          .sizes.medium?.source_url
                          ? post._embedded['wp:featuredmedia'][0].media_details
                              .sizes.medium.source_url
                          : post._embedded['wp:featuredmedia'][0].media_details
                              .sizes.full.source_url
                      }
                      title={post.title.rendered}
                      hat={post.acf.hat}
                      link={post.link}
                    />
                  </li>
                ) : (
                  ''
                )
              })}
            </ListVideos>
          </div>
        </section>
      </main>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const response = await fetch(`${api.production}/posts?_embed`)
  const posts = await response.json()

  return {
    props: {
      posts
    }
  }
}
