# Teste SEOX

Este projeto foi feito em Next.js para o processo seletivo do Kevin Oliveira para a empresa SEOX.

## Rodando o projeto

O projeto está hospedado na Vercel e pode ser acessado na seguinte url: [https://teste-frontend-kevin-oliveira.vercel.app/](https://teste-frontend-kevin-oliveira.vercel.app/)

Se quiser rodar localmente instale as dependências com `yarn` ou `npm install` na versão 18 e após isso rode no console:

```bash
npm run dev
# ou
yarn dev
# ou
pnpm dev
```

Abra [http://localhost:3000](http://localhost:3000) para ver o projeto rodando localmente no seu navegador.

## Dependências do projeto

Para mais informações acessar o arquivo [package.json](/package.json)

Dependência | Versão
--|--
Node | 18
Next | 13
Sass | 1.6
Commitzen | 3
Eslint | 8.4
Typescript | 5.1
styled-icons | 10.4

## Testes

### Unitários

Foi utilizado Jest e React Testing Library para testar o funcionamento dos componentes e a cobertura dos testes dos componentes pode ser acessada no terminal utilizando o comando `yarn test`.

### PageSpeed Insights

Testes feitos com a URL da Vercel, em mobile alcançamos a nota 99 e 100 em desktop, além das notas altas em acessibilidade, boas práticas e SEO.

![PageSpeed Insights - Mobile Version](/public/docs/pagespeed-mobile.png)

![PageSpeed Insights - Desktop Version](/public/docs/pagespeed-desktop.png)

## Créditos

Kevin Oliveira
[Site](https://kevinoliveira.com.br/) | [LinkedIn](https://linkedin.com/in/kvnol) | [GitHub](https://github.com/kvnol)
